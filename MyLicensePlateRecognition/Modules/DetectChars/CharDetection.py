import cv2
import math
import Constants
from Models.Char import Char

class CharDetection:
    def findPossibleCharsInPlate(imgGrayscale, imgThresh):
        listOfPossibleChars = []

        _, contours, _ = cv2.findContours(imgThresh.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        for contour in contours:
            potentialChar = Char(contour)

            if Char.isChar(potentialChar):
                listOfPossibleChars.append(potentialChar)
            # end if
        # end if

        return listOfPossibleChars
    # end function

    def findListOfListsOfMatchingChars(listOfPossibleChars):

        listOfListsOfMatchingChars = []

        for possibleChar in listOfPossibleChars:
            listOfMatchingChars = CharDetection.findListOfMatchingChars(possibleChar, listOfPossibleChars)
            listOfMatchingChars.append(possibleChar)

            if len(listOfMatchingChars) < Constants.MIN_NUMBER_OF_MATCHING_CHARS:
                continue
            # end if

            listOfListsOfMatchingChars.append(listOfMatchingChars)

            listOfPossibleCharsWithCurrentMatchesRemoved = list(set(listOfPossibleChars) - set(listOfMatchingChars))

            recursiveListOfListsOfMatchingChars = CharDetection.findListOfListsOfMatchingChars(listOfPossibleCharsWithCurrentMatchesRemoved)

            for recursiveListOfMatchingChars in recursiveListOfListsOfMatchingChars:
                listOfListsOfMatchingChars.append(
                    recursiveListOfMatchingChars)
            # end for

            break  # exit for

        # end for

        return listOfListsOfMatchingChars
        # end function

    def findListOfMatchingChars(potentialChar, listOfChars):
        listOfMatchingChars = []

        for possibleMatchingChar in listOfChars:
            if possibleMatchingChar == potentialChar:
                continue
            # end if
            distance_between_chars = CharDetection.distanceBetweenChars(potentialChar, possibleMatchingChar)

            angle_between_chars = CharDetection.angleBetweenChars(potentialChar, possibleMatchingChar)

            change_in_area = float(
                abs(possibleMatchingChar.boundingArea - potentialChar.boundingArea)) / float(potentialChar.boundingArea)

            change_in_width = float(
                abs(possibleMatchingChar.boundingWidth - potentialChar.boundingWidth)) / float(potentialChar.boundingWidth)
            change_in_height = float(
                abs(possibleMatchingChar.boundingHeight - potentialChar.boundingHeight)) / float(potentialChar.boundingHeight)

            # check if chars match
            if (distance_between_chars < (potentialChar.fltDiagonalLength * Constants.MAX_DIAG_SIZE_MULTIPLE_AWAY) and
                    angle_between_chars < Constants.MAX_ANGLE_BETWEEN_CHARS and
                    change_in_area < Constants.MAX_CHANGE_IN_AREA and
                    change_in_width < Constants.MAX_CHANGE_IN_WIDTH and
                    change_in_height < Constants.MAX_CHANGE_IN_HEIGHT):

                listOfMatchingChars.append(
                    possibleMatchingChar)
            # end if
        # end for

        return listOfMatchingChars
    # end function

    def distanceBetweenChars(firstChar, secondChar):
        y = abs(firstChar.intCenterY - secondChar.intCenterY)
        x = abs(firstChar.intCenterX - secondChar.intCenterX)

        return math.sqrt((y ** 2) + (x ** 2))
    # end function

    def angleBetweenChars(firstChar, secondChar):
        catetaOpusa = float(abs(firstChar.intCenterY - secondChar.intCenterY))
        catetaAlaturata = float(abs(firstChar.intCenterX - secondChar.intCenterX))

        if catetaAlaturata != 0.0:
            angleInRadians = math.atan(catetaOpusa / catetaAlaturata)
        else:
            angleInRadians = 1.5708
        # end if

        angleInDegrees = angleInRadians * (180.0 / math.pi)

        return angleInDegrees
    # end function