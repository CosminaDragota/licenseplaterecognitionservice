import cv2
from LocalizePlate.Preprocess import Preprocess
from .CharDetection import CharDetection
from RecognizeChars.CharRecognition import CharRecognition
import numpy as np
import random
import Constants

class CharDetectionController:
    def detectCharsInPlates(possible_plates_list):
        plate_counter = 0
        contours_list = []

        if len(possible_plates_list) == 0:
            return possible_plates_list
        # end if


        for possible_plate in possible_plates_list:

            possible_plate.imageGrayscaled, possible_plate.imageThresholded = Preprocess.preprocess(possible_plate.imagePlate)

            if Constants.SHOW_STEPS == True:
                cv2.imshow("Original plate", possible_plate.imagePlate)
                cv2.imshow("Grayscaled", possible_plate.imageGrayscaled)
                cv2.imshow("Thresholded", possible_plate.imageThresholded)

            possible_plate.imageThresholded = cv2.resize(possible_plate.imageThresholded, (0, 0), fx = 1.6, fy = 1.6)

            thresholdValue, possible_plate.imageThresholded = cv2.threshold(possible_plate.imageThresholded, 0.0, 255.0, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

            if Constants.SHOW_STEPS == True:
                cv2.imshow("Thresholded Resized", possible_plate.imageThresholded)

            list_of_possible_chars_in_plate = CharDetection.findPossibleCharsInPlate(possible_plate.imageGrayscaled, possible_plate.imageThresholded)

            if Constants.SHOW_STEPS == True:
                height, width, num_channels = possible_plate.imagePlate.shape
                img_contours = np.zeros((height, width, 3), np.uint8)
                del contours_list[:]

                for possible_char in list_of_possible_chars_in_plate:
                    contours_list.append(possible_char.contour)
                # end for

                cv2.drawContours(img_contours, contours_list, -1, Constants.SCALAR_WHITE)

                cv2.imshow("Contours", img_contours)
            # end if # show steps #####################################################################

            list_of_lists_of_matching_chars_in_plate = CharDetection.findListOfListsOfMatchingChars(list_of_possible_chars_in_plate)

            if Constants.SHOW_STEPS == True:
                img_contours = np.zeros((height, width, 3), np.uint8)
                del contours_list[:]

                for list_of_matching_chars in list_of_lists_of_matching_chars_in_plate:
                    random_blue = random.randint(0, 255)
                    random_green = random.randint(0, 255)
                    random_red = random.randint(0, 255)

                    for matching_char in list_of_matching_chars:
                        contours_list.append(matching_char.contour)
                    # end for
                    cv2.drawContours(img_contours, contours_list, -1, (random_blue, random_green, random_red))
                # end for
                cv2.imshow("Matching contours", img_contours)

            if (len(list_of_lists_of_matching_chars_in_plate) == 0):

                if Constants.SHOW_STEPS == True:
                    print("plate number " + str(plate_counter) + "has chars, click on an image and press a key")
                    plate_counter = plate_counter + 1
                    cv2.destroyWindow("Matching contours without overlapping chars")
                    cv2.destroyWindow("Longest list of chars")
                    cv2.destroyWindow("Individual characters identified")
                    #cv2.waitKey(0)

                possible_plate.strChars = ""
                continue
            # end if

            for i in range(0, len(list_of_lists_of_matching_chars_in_plate)):
                list_of_lists_of_matching_chars_in_plate[i].sort(key = lambda matchingChar: matchingChar.intCenterX)
                list_of_lists_of_matching_chars_in_plate[i] = CharDetectionController.removeInnerOverlappingChars(list_of_lists_of_matching_chars_in_plate[i])
            # end for

            if Constants.SHOW_STEPS == True:
                img_contours = np.zeros((height, width, 3), np.uint8)

                for list_of_matching_chars in list_of_lists_of_matching_chars_in_plate:
                    random_blue = random.randint(0, 255)
                    random_red = random.randint(0, 255)
                    random_green = random.randint(0, 255)

                    del contours_list[:]

                    for matching_char in list_of_matching_chars:
                        contours_list.append(matching_char.contour)
                    # end for

                    cv2.drawContours(img_contours, contours_list, -1, (random_blue, random_green, random_red))
                # end for
                cv2.imshow("Matching contours without overlapping chars", img_contours)

            length_of_longest_list_of_chars = 0
            index_of_longest_list_of_chars = 0

            for i in range(0, len(list_of_lists_of_matching_chars_in_plate)):
                if len(list_of_lists_of_matching_chars_in_plate[i]) > length_of_longest_list_of_chars:
                    length_of_longest_list_of_chars = len(list_of_lists_of_matching_chars_in_plate[i])
                    index_of_longest_list_of_chars = i
                # end if
            # end for

            longest_list_of_matching_chars_in_plate = list_of_lists_of_matching_chars_in_plate[index_of_longest_list_of_chars]

            if Constants.SHOW_STEPS == True: # show steps ###################################################
                img_contours = np.zeros((height, width, 3), np.uint8)
                del contours_list[:]

                for matching_char in longest_list_of_matching_chars_in_plate:
                    contours_list.append(matching_char.contour)
                # end for

                cv2.drawContours(img_contours, contours_list, -1, Constants.SCALAR_WHITE)

                cv2.imshow("Longest list of chars", img_contours)

            possible_plate.strChars = CharRecognition.recognizeCharsInPlate(possible_plate.imageThresholded, longest_list_of_matching_chars_in_plate)

            if Constants.SHOW_STEPS == True: # show steps ###################################################
                print("plate number " + str(plate_counter) + " has chars= " + possible_plate.strChars + ", click on an image and press a key")
                plate_counter = plate_counter + 1
                #cv2.waitKey(0)
            # end if


        if Constants.SHOW_STEPS == True:
            print("\nchar detection complete, click on an image and press a key \n")
            #cv2.waitKey(0)
        # end if

        return possible_plates_list
    # end function


    def removeInnerOverlappingChars(listOfMatchingChars):
        listOfMatchingCharsWithInnerCharRemoved = list(listOfMatchingChars)

        for currentChar in listOfMatchingChars:
            for otherChar in listOfMatchingChars:
                if currentChar != otherChar:

                    if CharDetection.distanceBetweenChars(currentChar, otherChar) < (currentChar.fltDiagonalLength * Constants.MIN_DIAG_SIZE_MULTIPLE_AWAY):
                        if currentChar.boundingArea < otherChar.boundingArea:
                            if currentChar in listOfMatchingCharsWithInnerCharRemoved:
                                listOfMatchingCharsWithInnerCharRemoved.remove(currentChar)
                            # end if
                        else:
                            if otherChar in listOfMatchingCharsWithInnerCharRemoved:
                                listOfMatchingCharsWithInnerCharRemoved.remove(otherChar)
                            # end if
                        # end if
                    # end if
                # end if
            # end for
        # end for

        return listOfMatchingCharsWithInnerCharRemoved
    # end function


