import Constants
import cv2
import time

class Postprocess:
    def findActualLicensePlateOnImage(original_image, list_of_possible_plates):
            if len(list_of_possible_plates) == 0:
                print("\nno license plates were detected\n")
                return
            else:  # else

                list_of_possible_plates.sort(key=lambda possiblePlate: len(possiblePlate.strChars), reverse=True)

                licPlate = list_of_possible_plates[0]

                #cv2.imshow("Crop of plate", licPlate.imagePlate)
                #cv2.imshow("Thresholded crop of plate", licPlate.imageThresholded)

                if len(licPlate.strChars) == 0:
                    print("\nPlate has 0 characters\n\n")
                    return  # and exit program
                # end if

                Postprocess.drawRedRectangleAroundPlate(original_image, licPlate)

                Postprocess.writeLicensePlateCharsOnImage(original_image, licPlate)

                cv2.imshow("nimic",original_image)

                #cv2.imwrite(args["output"], licPlate.imagePlate)

                # end if else

            #cv2.waitKey(0)

            return licPlate
        # end main

    def drawRedRectangleAroundPlate(original_image, actualPlate):

        rect_points = cv2.boxPoints(actualPlate.rotatedRectangleOfPlate)

        cv2.line(original_image, tuple(rect_points[0]), tuple(rect_points[1]), Constants.SCALAR_RED,
                 2)
        cv2.line(original_image, tuple(rect_points[1]), tuple(rect_points[2]), Constants.SCALAR_RED, 2)
        cv2.line(original_image, tuple(rect_points[2]), tuple(rect_points[3]), Constants.SCALAR_RED, 2)
        cv2.line(original_image, tuple(rect_points[3]), tuple(rect_points[0]), Constants.SCALAR_RED, 2)

    # end function

    def writeLicensePlateCharsOnImage(original_image, actualPlate):

        scene_height, scene_width, scene_num_channels = original_image.shape
        plate_height, plate_width, plate_num_channels = actualPlate.imagePlate.shape

        font_face = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = float(plate_height) / 30.0
        font_thickness = int(round(font_scale * 1.5))

        text_size, baseline = cv2.getTextSize(actualPlate.strChars, font_face, font_scale,
                                             font_thickness)

        ((int_plate_center_x, int_plate_center_y), (_, _),
         correction_angle_in_deg) = actualPlate.rotatedRectangleOfPlate

        int_plate_center_x = int(int_plate_center_x)
        int_plate_center_y = int(int_plate_center_y)

        pt_center_of_text_area_x = int(int_plate_center_x)

        if int_plate_center_y < (scene_height * 0.75):
            pt_center_of_text_area_y = int(round(int_plate_center_y)) + int(
                round(plate_height * 1.6))
        else:
            pt_center_of_text_area_y = int(round(int_plate_center_y)) - int(
                round(plate_height * 1.6))
        # end if

        text_size_width, text_size_height = text_size

        pt_lower_left_text_origin_x = int(
            pt_center_of_text_area_x - (text_size_width / 2))
        pt_lower_left_text_origin_y = int(
            pt_center_of_text_area_y + (text_size_height / 2))

        cv2.putText(original_image, actualPlate.strChars, (pt_lower_left_text_origin_x, pt_lower_left_text_origin_y), font_face,
                    font_scale, Constants.SCALAR_YELLOW, font_thickness)
    # end function