import numpy as np
import cv2
#from Char import Char
import Constants
import math
from Models.Char import Char
from Models.Plate import Plate

class PlateDetection:
    def findPossibleCharsInScene(imgThresh):
        possible_chars_list = []

        possible_chars_counter = 0

        _, contours, _ = cv2.findContours(imgThresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        height, width = imgThresh.shape
        image_contours = np.zeros((height, width, 3), np.uint8)

        for i in range(0, len(contours)):

            cv2.drawContours(image_contours, contours, i, Constants.SCALAR_WHITE)

            potential_char = Char(contours[i])

            if Char.isChar(potential_char):
                possible_chars_counter = possible_chars_counter + 1
                possible_chars_list.append(potential_char)
            # end if
        # end for

        if Constants.SHOW_STEPS == True:
            print("\nContours length= " + str(len(contours)))
            print("chars counter = " + str(possible_chars_counter))
            cv2.imshow("Contours", image_contours)

        return possible_chars_list
    # end function
    def getPlate(original_image, list_of_matching_chars):
        potential_plate = Plate()

        list_of_matching_chars.sort(key=lambda charMatch: charMatch.intCenterX)

        centerXofPlate = (list_of_matching_chars[0].intCenterX + list_of_matching_chars[len(list_of_matching_chars) - 1].intCenterX) / 2.0
        centerYofPlate = (list_of_matching_chars[0].intCenterY + list_of_matching_chars[len(list_of_matching_chars) - 1].intCenterY) / 2.0

        centerPointOfPlate = centerXofPlate, centerYofPlate

        widthOfPlate = int((list_of_matching_chars[len(list_of_matching_chars) - 1].boundingX + list_of_matching_chars[len(list_of_matching_chars) - 1].boundingWidth - list_of_matching_chars[0].boundingX) * Constants.PLATE_WIDTH_PADDING_FACTOR)

        total_of_char_heights = 0

        for matching_char in list_of_matching_chars:
            total_of_char_heights = total_of_char_heights + matching_char.boundingHeight
        # end for

        average_height_of_char = total_of_char_heights / len(list_of_matching_chars)

        heightOfPlate = int(average_height_of_char * Constants.PLATE_HEIGHT_PADDING_FACTOR)

        cateta_opusa = list_of_matching_chars[len(list_of_matching_chars) - 1].intCenterY - list_of_matching_chars[0].intCenterY
        ipotenuza = Char.distanceBetweenChars(list_of_matching_chars[0], list_of_matching_chars[len(list_of_matching_chars) - 1])
        correction_angle_in_radians = math.asin(cateta_opusa / ipotenuza)
        correction_angle_in_degrees = correction_angle_in_radians * (180.0 / math.pi)

        potential_plate.rotatedRectangleOfPlate = (tuple(centerPointOfPlate), (widthOfPlate, heightOfPlate), correction_angle_in_degrees)

        rotation_matrix = cv2.getRotationMatrix2D(tuple(centerPointOfPlate), correction_angle_in_degrees, 1.0)

        height, width, _ = original_image.shape

        img_rotated = cv2.warpAffine(original_image, rotation_matrix, (width, height))

        img_cropped = cv2.getRectSubPix(img_rotated, (widthOfPlate, heightOfPlate), tuple(centerPointOfPlate))

        potential_plate.imagePlate = img_cropped

        return potential_plate
    # end function

    def findListOfListsOfMatchingChars(list_of_possible_chars):

        list_of_lists_of_matching_chars = []

        for possible_char in list_of_possible_chars:
            list_of_matching_chars = PlateDetection.findListOfMatchingChars(possible_char,
                                                                         list_of_possible_chars)

            list_of_matching_chars.append(possible_char)

            if len(list_of_matching_chars) < Constants.MIN_NUMBER_OF_MATCHING_CHARS:
                continue
            # end if

            list_of_lists_of_matching_chars.append(list_of_matching_chars)

            list_of_possible_chars_with_current_matches_removed = list(set(list_of_possible_chars) - set(list_of_matching_chars))

            recursive_list_of_lists_of_matching_chars = PlateDetection.findListOfListsOfMatchingChars(
                list_of_possible_chars_with_current_matches_removed)  # recursive call

            for recursive_list_of_matching_chars in recursive_list_of_lists_of_matching_chars:
                list_of_lists_of_matching_chars.append(
                    recursive_list_of_matching_chars)
            # end for

            break  # exit for

        # end for

        return list_of_lists_of_matching_chars
        # end function

    def findListOfMatchingChars(potential_char, list_of_chars):

        list_of_matching_chars = []

        for possible_matching_char in list_of_chars:
            if possible_matching_char == potential_char:
                continue
            # end if
            distance_between_chars = PlateDetection.distanceBetweenChars(potential_char, possible_matching_char)

            angle_between_chars = PlateDetection.angleBetweenChars(potential_char, possible_matching_char)

            change_in_area = float(
                abs(possible_matching_char.boundingArea - potential_char.boundingArea)) / float(
                potential_char.boundingArea)

            change_in_width = float(
                abs(possible_matching_char.boundingWidth - potential_char.boundingWidth)) / float(
                potential_char.boundingWidth)
            change_in_height = float(
                abs(possible_matching_char.boundingHeight - potential_char.boundingHeight)) / float(
                potential_char.boundingHeight)

            if (distance_between_chars < (potential_char.fltDiagonalLength * Constants.MAX_DIAG_SIZE_MULTIPLE_AWAY) and
                    angle_between_chars < Constants.MAX_ANGLE_BETWEEN_CHARS and
                    change_in_area < Constants.MAX_CHANGE_IN_AREA and
                    change_in_width < Constants.MAX_CHANGE_IN_WIDTH and
                    change_in_height < Constants.MAX_CHANGE_IN_HEIGHT):

                list_of_matching_chars.append(
                    possible_matching_char)
            # end if
        # end for

        return list_of_matching_chars
    # end function

    def distanceBetweenChars(firstChar, secondChar):
        y = abs(firstChar.intCenterY - secondChar.intCenterY)
        x = abs(firstChar.intCenterX - secondChar.intCenterX)

        return math.sqrt((y ** 2) + (x ** 2))

    # end function

    def angleBetweenChars(firstChar, secondChar):
        catetaOpusa = float(abs(firstChar.intCenterY - secondChar.intCenterY))
        catetaAlaturata = float(abs(firstChar.intCenterX - secondChar.intCenterX))

        if catetaAlaturata != 0.0:
            angleInRadians = math.atan(catetaOpusa / catetaAlaturata)
        else:
            angleInRadians = 1.5708
        # end if

        angleInDegrees = angleInRadians * (180.0 / math.pi)

        return angleInDegrees
    # end function