import cv2
import random
import numpy as np
import Constants
from .Preprocess import Preprocess
from .PlateDetection import PlateDetection

class PlateLocalizationController:
    def detectPlatesInScene(original_image):
        list_of_possible_plates = []

        height, width, num_channels = original_image.shape

        image_contours = np.zeros((height, width, 3), np.uint8)

        cv2.destroyAllWindows()

        if Constants.SHOW_STEPS == True:
            cv2.imshow("Original window", original_image)

        img_grayscale_scene, img_thresh_scene = Preprocess.preprocess(original_image)

        if Constants.SHOW_STEPS == True:

            cv2.imshow("Grayscale", img_grayscale_scene)

            cv2.imshow("Threshold", img_thresh_scene)

        list_of_possible_chars_in_scene = PlateDetection.findPossibleCharsInScene(img_thresh_scene)

        if Constants.SHOW_STEPS == True:
            print("len(list_of_possible_chars_in_scene) = " + str(
                len(list_of_possible_chars_in_scene)))

            image_contours = np.zeros((height, width, 3), np.uint8)

            contours = []

            for possible_char in list_of_possible_chars_in_scene:
                contours.append(possible_char.contour)
            # end for

            cv2.drawContours(image_contours, contours, -1, Constants.SCALAR_GREEN)

            cv2.imshow("Possible chars", image_contours)

        list_of_lists_of_matching_chars_in_scene = PlateDetection.findListOfListsOfMatchingChars(list_of_possible_chars_in_scene)

        if Constants.SHOW_STEPS == True:
            print("Count of list_of_lists_of_matching_chars_in_scene= " + str(
                len(list_of_lists_of_matching_chars_in_scene)))

            image_contours = np.zeros((height, width, 3), np.uint8)

            for list_of_matching_chars in list_of_lists_of_matching_chars_in_scene:
                random_blue = random.randint(0, 255)
                random_red = random.randint(0, 255)
                random_green = random.randint(0, 255)

                contours = []

                for matching_char in list_of_matching_chars:
                    contours.append(matching_char.contour)
                # end for

                cv2.drawContours(image_contours, contours, -1, (random_blue, random_green, random_red))
            # end for

            cv2.imshow("List of groups of matching chars", image_contours)

        for list_of_matching_chars in list_of_lists_of_matching_chars_in_scene:
            potential_plate = PlateDetection.getPlate(original_image, list_of_matching_chars)

            if potential_plate.imagePlate is not None:
                list_of_possible_plates.append(potential_plate)
            # end if
        # end for

        print("\nThere are " + str(len(list_of_possible_plates)) + " possible plates")

        if Constants.SHOW_STEPS == True:
            print("\n")
            cv2.imshow("Image contours", image_contours)

            for i in range(0, len(list_of_possible_plates)):
                rectangle_points = cv2.boxPoints(list_of_possible_plates[i].rotatedRectangleOfPlate)

                cv2.line(image_contours, tuple(rectangle_points[0]), tuple(rectangle_points[1]), Constants.SCALAR_RED, 2)
                cv2.line(image_contours, tuple(rectangle_points[1]), tuple(rectangle_points[2]), Constants.SCALAR_RED, 2)
                cv2.line(image_contours, tuple(rectangle_points[2]), tuple(rectangle_points[3]), Constants.SCALAR_RED, 2)
                cv2.line(image_contours, tuple(rectangle_points[3]), tuple(rectangle_points[0]), Constants.SCALAR_RED, 2)

                cv2.imshow("All contours", image_contours)

                print("possible plate " + str(i) + ", click on an image and press a key")

                cv2.imshow("Possible plate", list_of_possible_plates[i].imagePlate)

                #cv2.waitKey(0)
            # end for

            print("\nplate detection complete, click on an image and press a key to begin char recognition.\n")
            #cv2.waitKey(0)

        return list_of_possible_plates