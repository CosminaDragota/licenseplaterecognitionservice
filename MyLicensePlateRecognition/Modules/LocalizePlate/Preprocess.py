import cv2
import Constants


class Preprocess:
    def preprocess(original_image):
        grayscale_image = Preprocess.extractGrayscaleImage(original_image)

        max_contrast_grayscale = Preprocess.maximizeContrast(grayscale_image)

        blurred_image = cv2.GaussianBlur(max_contrast_grayscale, Constants.GAUSSIAN_SMOOTH_KERNEL_SIZE, 0)

        thresholded_image = cv2.adaptiveThreshold(blurred_image
                                                  , 255.0
                                                  , cv2.ADAPTIVE_THRESH_GAUSSIAN_C
                                                  , cv2.THRESH_BINARY_INV
                                                  , Constants.ADAPTIVE_THRESH_NEIGHBOURHOOD
                                                  , Constants.ADAPTIVE_THRESH_CONSTANT)

        return grayscale_image, thresholded_image

    def extractGrayscaleImage(original_image):

        hsv_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2HSV)

        hue, saturation, value = cv2.split(hsv_image)

        return value

    def maximizeContrast(grayscale_image):

        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

        img_top_hat = cv2.morphologyEx(grayscale_image, cv2.MORPH_TOPHAT, kernel)
        img_black_hat = cv2.morphologyEx(grayscale_image, cv2.MORPH_BLACKHAT, kernel)

        img_grayscale_plus_top_hat = cv2.add(grayscale_image, img_top_hat)
        img_grayscale_plus_top_hat_minus_black_hat = cv2.subtract(img_grayscale_plus_top_hat, img_black_hat)
        return img_grayscale_plus_top_hat_minus_black_hat


