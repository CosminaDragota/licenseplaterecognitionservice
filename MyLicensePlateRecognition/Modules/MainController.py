import cv2
import os
import argparse
from LocalizePlate import PlateLocalizationController as plc
from DetectChars import CharDetectionController as cdc
from RecognizeChars import CharRecognition as cr
from DetectChars import Postprocess as pp

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
                help="path to input image")
args = vars(ap.parse_args())

class MainController:
    def start(self):
        original_image = cv2.imread(args['input'], 1)

        if original_image is None:
            print("\n Error: could not read image!", original_image)
            os.system("pause")
            return None
        else:
            print("Image read successfully!")
            blnKNNTrainingSuccessful = cr.CharRecognition.loadKNNDataAndTrainKNN()

            if blnKNNTrainingSuccessful == False:
                print("\nerror: KNN traning was not successful\n")
                return
            # end if
            else:
                listOfPossiblePlates = plc.PlateLocalizationController.detectPlatesInScene(original_image)
                listOfPossiblePlates = cdc.CharDetectionController.detectCharsInPlates(listOfPossiblePlates)
                print("End")
                licensePlate = pp.Postprocess.findActualLicensePlateOnImage(original_image, listOfPossiblePlates)
                if(licensePlate!=None):
                    cv2.imwrite("D:/FACULTA/LICENTA/repo/Server/Server/App_Data/plate.jpg", licensePlate.imagePlate)
                    print(licensePlate.strChars)
                    path = 'D:/FACULTA/LICENTA/repo/Server/Server/App_Data/charsOnPlate.txt'
                    charsOnPlate_file = open(path, 'w')
                    charsOnPlate_file.write(licensePlate.strChars)
                else:
                    noPlatesImage = cv2.imread("D:/FACULTA/LICENTA/repo/Server/Server/App_Data/noPlates.jpg", 1)
                    cv2.imwrite("D:/FACULTA/LICENTA/repo/Server/Server/App_Data/plate.jpg", noPlatesImage)
                    print("No plates")
                    path = 'D:/FACULTA/LICENTA/repo/Server/Server/App_Data/charsOnPlate.txt'
                    charsOnPlate_file = open(path, 'w')
                    charsOnPlate_file.write("no plates")


if __name__ == '__main__':
    main = MainController()
    main.start()
