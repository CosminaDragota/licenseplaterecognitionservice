import cv2
import math
import Constants

class Char:

    def __init__(self, contour):
        self.contour = contour

        self.boundingBox = cv2.boundingRect(self.contour)

        [x, y, width, height] = self.boundingBox

        self.boundingX = x
        self.boundingY = y
        self.boundingWidth = width
        self.boundingHeight = height

        self.fltDiagonalLength = math.sqrt((self.boundingWidth ** 2) + (self.boundingHeight ** 2))

        self.fltAspectRatio = float(self.boundingWidth) / float(self.boundingHeight)

        self.boundingArea = self.boundingWidth * self.boundingHeight

        self.intCenterX = (2 * self.boundingX + self.boundingWidth) / 2
        self.intCenterY = (2 * self.boundingY + self.boundingHeight) / 2
    # end constructor

    def isChar(possibleChar):

        if (possibleChar.boundingArea > Constants.MIN_PIXEL_AREA and
                possibleChar.boundingWidth > Constants.MIN_PIXEL_WIDTH and
                possibleChar.boundingHeight > Constants.MIN_PIXEL_HEIGHT and
                possibleChar.fltAspectRatio > Constants.MIN_ASPECT_RATIO and
                possibleChar.fltAspectRatio < Constants.MAX_ASPECT_RATIO):
            return True
        else:
            return False
        # end if
    # end function

    def distanceBetweenChars(firstChar, secondChar):
        intY = abs(firstChar.intCenterY - secondChar.intCenterY)
        intX = abs(firstChar.intCenterX - secondChar.intCenterX)

        return math.sqrt((intY ** 2) + (intX ** 2))
# end class








