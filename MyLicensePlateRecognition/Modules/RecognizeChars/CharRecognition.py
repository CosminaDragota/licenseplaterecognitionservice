import cv2
import os
import numpy as np
import Constants

kNearest = cv2.ml.KNearest_create()

class CharRecognition:
    def recognizeCharsInPlate(thresholded_image, list_of_matching_chars):
        str_chars = ""

        height, width = thresholded_image.shape

        img_thresh_color = np.zeros((height, width, 3), np.uint8)

        list_of_matching_chars.sort(key = lambda matching_char: matching_char.intCenterX)

        cv2.cvtColor(thresholded_image, cv2.COLOR_GRAY2BGR, img_thresh_color)

        for current_char in list_of_matching_chars:
            point_one = (current_char.boundingX, current_char.boundingY)
            point_two = \
            ((current_char.boundingX + current_char.boundingWidth), (current_char.boundingY + current_char.boundingHeight))

            cv2.rectangle(img_thresh_color, point_one, point_two, Constants.SCALAR_GREEN, 2)

            region_of_interest = thresholded_image[current_char.boundingY: current_char.boundingY + current_char.boundingHeight,
                     current_char.boundingX: current_char.boundingX + current_char.boundingWidth]

            region_of_interest_resized = cv2.resize(region_of_interest, (Constants.RESIZED_CHAR_IMAGE_WIDTH,
                                                Constants.RESIZED_CHAR_IMAGE_HEIGHT))

            npa_region_of_interest_resized = region_of_interest_resized.reshape((1,
                                                   Constants.RESIZED_CHAR_IMAGE_WIDTH * Constants.RESIZED_CHAR_IMAGE_HEIGHT))

            npa_region_of_interest_resized = np.float32(npa_region_of_interest_resized)

            _, npa_results, _, _ = kNearest.findNearest(npa_region_of_interest_resized,
                                                                   k=1)

            str_current_char = str(chr(int(npa_results[0][0])))  #

            str_chars = str_chars + str_current_char
        # end for

        if Constants.SHOW_STEPS == True:
            cv2.imshow("Individual characters identified", img_thresh_color)

        return str_chars
    # end function

    def loadKNNDataAndTrainKNN():

        try:
            classifications = np.loadtxt("D:/FACULTA/LICENTA/LPRrepo/MyLicensePlateRecognition/Modules/RecognizeChars/classifications.txt",
                                            np.float32)
        except:
            print("could not open file classifications.txt, exiting program\n")
            os.system("pause")
            return False  # and return False
        # end try

        try:
            flattened_images = np.loadtxt("D:/FACULTA/LICENTA/LPRrepo/MyLicensePlateRecognition/Modules/RecognizeChars/flattened_images.txt", np.float32)
        except:
            print("could not open file flattened_images.txt, exiting program\n")
            os.system("pause")
            return False
        # end try

        classifications = classifications.reshape(
            (classifications.size, 1))

        kNearest.setDefaultK(1)

        kNearest.train(flattened_images, cv2.ml.ROW_SAMPLE, classifications)

        return True
    # end function