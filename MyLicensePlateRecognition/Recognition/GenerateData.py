import sys
import cv2
import os
import numpy as np
import Constants

def start():
    testImage = cv2.imread("train/test2.png")

    if testImage is None:
        print("error: image not read from file \n\n")
        os.system("pause")
        return
    # end if

    imgGray = cv2.cvtColor(testImage, cv2.COLOR_BGR2GRAY)
    imgBlurred = cv2.GaussianBlur(imgGray, (5, 5), 0)

    imgThresh = cv2.adaptiveThreshold(imgBlurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)

    cv2.imshow("imgThresh", imgThresh)

    imgContours, npaContours, npaHierarchy = cv2.findContours(imgThresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    flatenedImages =  np.empty((0, Constants.IMAGE_WIDTH * Constants.IMAGE_HEIGHT))

    classifications = []

    for npaContour in npaContours:
        if cv2.contourArea(npaContour) > Constants.MIN_AREA:
            [x, y, w, h] = cv2.boundingRect(npaContour)

            cv2.rectangle(testImage, (x, y), (x+w,y+h),(0, 0, 255),2)

            imgROI = imgThresh[y:y+h, x:x+w]
            imgROIResized = cv2.resize(imgROI, (Constants.IMAGE_WIDTH, Constants.IMAGE_HEIGHT))

            cv2.imshow("imgROI", imgROI)
            cv2.imshow("imgROIResized", imgROIResized)
            cv2.imshow("testImage.png", testImage)

            intChar = cv2.waitKey(0)

            validChars = [ord('0'), ord('1'), ord('2'), ord('3'), ord('4'), ord('5'), ord('6'), ord('7'), ord('8'),
                          ord('9'),
                          ord('A'), ord('B'), ord('C'), ord('D'), ord('E'), ord('F'), ord('G'), ord('H'), ord('I'),
                          ord('J'),
                          ord('K'), ord('L'), ord('M'), ord('N'), ord('O'), ord('P'), ord('Q'), ord('R'), ord('S'),
                          ord('T'),
                          ord('U'), ord('V'), ord('W'), ord('X'), ord('Y'), ord('Z')]

            if intChar == 27:
                sys.exit()
            elif intChar in validChars:
                print("Valid char")
                classifications.append(intChar)

                npaFlattenedImage = imgROIResized.reshape((1, Constants.IMAGE_WIDTH * Constants.IMAGE_HEIGHT))
                print("\nLength of flatened image ", len(npaFlattenedImage))
                flatenedImages = np.append(flatenedImages, npaFlattenedImage, 0)
            # end if
        # end if
    # end for
    print("\nLength of flatened images ", len(flatenedImages))
    fltClassifications = np.array(classifications, np.float32)

    npaClassifications = fltClassifications.reshape((fltClassifications.size, 1))

    print("\nLength", len(npaClassifications))
    for j in range(npaClassifications.shape[0]):
        print(npaClassifications[j])

    print("\n\ntraining complete !!\n")

    np.savetxt("classifications.txt", npaClassifications)
    np.savetxt("flattened_images.txt", flatenedImages)

    cv2.destroyAllWindows()

    return

if __name__ == "__main__":
    start()
# end if




