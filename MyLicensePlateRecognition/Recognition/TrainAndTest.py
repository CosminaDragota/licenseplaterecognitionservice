import operator
import os
import Constants
import numpy as np
import cv2
from sklearn.metrics import accuracy_score


class ContourWithData():

    contour = None
    boundingRect = None
    xCoord = 0
    yCoord = 0
    width = 0
    height = 0
    area = 0.0

    def rectangleComputing(self):
        [x, y, width, height] = self.boundingRect
        self.xCoord = x
        self.yCoord = y
        self.width = width
        self.height = height

    def contourIsValid(self):
        if self.area < Constants.MIN_AREA : return False
        return True

def start():
    allContours = []
    validContours = []
    results=[]

    try:
        classifications = np.loadtxt("classifications.txt", np.float32)
    except:
        print("error opening file classifications.txt\n")
        os.system("pause")
        return
    # end try

    try:
        flattenedImages = np.loadtxt("flattened_images.txt", np.float32)
    except:
        print("error opening file flattened_images.txt\n")
        os.system("pause")
        return
    # end try

    classifications = classifications.reshape((classifications.size, 1))
    print(classifications)

    kNearest = cv2.ml.KNearest_create()

    kNearest.train(flattenedImages, cv2.ml.ROW_SAMPLE, classifications) # train data, ..., responses

    testImage = cv2.imread("train/test2.png")

    if testImage is None:
        print("error: image not read from file \n\n")
        os.system("pause")
        return
    # end if

    imgGray = cv2.cvtColor(testImage, cv2.COLOR_BGR2GRAY)
    imgBlurred = cv2.GaussianBlur(imgGray, (5,5), 0)


    imgThresh = cv2.adaptiveThreshold(imgBlurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV, 11, 2)

    imgContours, npaContours, npaHierarchy = cv2.findContours(imgThresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in npaContours:
        contourWithData = ContourWithData()
        contourWithData.contour = contour
        contourWithData.boundingRect = cv2.boundingRect(contourWithData.contour)
        contourWithData.rectangleComputing()
        contourWithData.area = cv2.contourArea(contourWithData.contour)
        allContours.append(contourWithData)
    # end for

    for contourWithData in allContours:
        if contourWithData.contourIsValid():
            validContours.append(contourWithData)
        # end if
    # end for

    validContours.sort(key = operator.attrgetter("xCoord"))

    strFinalString = ""

    for contourWithData in validContours:

        cv2.rectangle(testImage,
                      (contourWithData.xCoord, contourWithData.yCoord),
                      (contourWithData.xCoord + contourWithData.width, contourWithData.yCoord + contourWithData.height),
                      (0, 255, 0),
                      2)

        imgROI = imgThresh[contourWithData.yCoord: contourWithData.yCoord + contourWithData.height,
                 contourWithData.xCoord: contourWithData.xCoord + contourWithData.width]

        imgROIResized = cv2.resize(imgROI, (Constants.IMAGE_WIDTH, Constants.IMAGE_HEIGHT))

        npaROIResized = imgROIResized.reshape((1, Constants.IMAGE_WIDTH * Constants.IMAGE_HEIGHT))

        npaROIResized = np.float32(npaROIResized)

        retval, npaResults, neigh_resp, dists = kNearest.findNearest(npaROIResized, k = 1) # newcomer

        results.append(npaResults)
        print(npaResults)
        matches = npaResults == classifications
        correct = np.count_nonzero(matches)
        accuracy = correct * 100.0 / len(results)
        print("result size - ", neigh_resp.size)
        print("accuracy")
        print(accuracy)

        strCurrentChar = str(chr(int(npaResults[0][0])))

        strFinalString = strFinalString + strCurrentChar
    # end for

    print("\nString:" + strFinalString + "\n")

    cv2.imshow("imgTestingNumbers", testImage)
    cv2.waitKey(0)

    cv2.destroyAllWindows()

    return

###################################################################################################
if __name__ == "__main__":
    start()
# end if









